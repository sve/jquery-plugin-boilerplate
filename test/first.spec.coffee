jasmine.getFixtures().fixturesPath = 'base/test/fixtures'

describe "Plugin", ->

  beforeEach(->
    loadFixtures 'default.html'
    $j(".block").pluginName()
  )

  it 'should have div element', ->
    expect($j('.block')).toBeInDOM()

  it 'should have span element', ->
    expect($j('.block').find("span")).toBeInDOM()

  it 'changes settings.name', ->
    loadFixtures 'name.html'
    text = $j(".block").text()
    $j(".block").pluginName()
    expect($j('.block span').html()).toBe(text)