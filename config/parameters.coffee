app = "./app/"
cfg = 
  app: app
  build: "build/"
  dist: "dist/"
  test: "test/"
  scripts: app + "scripts/"
  styles: app + "styles/"

module.exports = cfg