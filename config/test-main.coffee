allTestFiles = []
TEST_REGEXP = /(spec|test)(\.coffee)?(\.js)?$/i
pathToModule = (path) ->
  path.replace(/^\/base\//, "").replace(/\.js$/, "").replace(/\.cofee$/, "")

Object.keys(window.__karma__.files).forEach (file) ->
  # Normalize paths to RequireJS module names.
  allTestFiles.push pathToModule(file)  if TEST_REGEXP.test(file)
  return

requirejs.config
  # Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base'
  deps: allTestFiles
  paths:
    'jquery': '../bower_components/jquery/dist/jquery'
  #   'scrollbar': '../bower_components/jquery.scrollbar/jquery.scrollbar'
  # shim: 
  #   'scrollbar': 
  #     deps: ['jquery']
  #     exports: 'scrollbar'
  callback: window.__karma__.start