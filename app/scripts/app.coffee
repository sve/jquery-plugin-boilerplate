do ($ = jQuery, window, document) ->
  # Create the defaults once
  pluginName = "pluginName"
  defaults =
    property: "value"
    name: "jQuery Plugin Boilerplate"

  # The actual plugin constructor
  class Plugin
    constructor: (@element, options) ->
      @settings = $.extend true, {}, defaults, options
      @_defaults = defaults
      @_name = pluginName
      @_init()

    _init: ->
      @el = $ @element
      @settings.name = @el.text() || @settings.name
      @_build()

    _build: ->
      @el.html @publicMethod()

    publicMethod: ->
      $ "<span/>"
        .addClass pluginName
        .html @settings.name
      

  $.fn[pluginName] = (options) ->
    args = arguments
    if options == undefined or typeof options == "object"
      return @each ->
        unless $.data(this, "plugin_" + pluginName)
          $.data this, "plugin_" + pluginName, new Plugin(this, options)
    else if typeof options == "string" and options[0] != "_"
      returns = undefined
      @each ->
        instance = $.data(this, "plugin_" + pluginName)
        if instance instanceof Plugin and typeof instance[options] == "function"
          returns = instance[options].apply(instance, Array::slice.call(args, 1))
        if options == "destroy"
          $.data this, "plugin_" + pluginName, null
        return
      return if returns != undefined then returns else this